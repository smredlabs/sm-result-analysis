# README #

## How to install? ##
1. Install Python 2.7
1. Download [PDFMiner](https://pypi.python.org/pypi/pdfminer/20140328). ((Python 3 is not supported.))
1. Unpack it.
1. Run setup.py to install:
        ```
        $ python setup.py install
        ```
2. Download [Setuptools](https://pypi.python.org/pypi/setuptools#code-of-conduct).
1. Unpack it.
1. Run setup.py to install:
        ```
        $ python setup.py install
        ```
5. Download [Xlwt](https://pypi.python.org/pypi/xlwt).
6. Unpack it.
7. Run setup.py to install:
        ```
        $ python setup.py install
        ```


Download PDF files of students of a particular semester and save it in a common folder. For example, if S6 B.Tech results are published, create a folder "sem6" and download the PDF files of each student into that folder. Rename the downloaded PDF file as "1.pdf" if the student registration number is JYAFECS001. If registration number is JYAFECS059, then rename it as 59.pdf.

**Now download sm_resultAnalysis from Downloads of Bitbucket.**

**Next Run SM Result Analysis as shown below:**

```
$ python sm_resultAnalysis.py path_to_folder_having_pdfs
```

**If successful, an excel file named "results.xls" will be generated.**
